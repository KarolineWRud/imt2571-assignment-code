<?php
include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books. 
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{   
/**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;  
    
    /**
	 * @throws PDOException
     */
    public function __construct($db = null)  
    {  
	    if ($db)   // If a PDO-object for db exists
		{
			$this->db = $db;     // Sets existing db as database
		}
		else      // if no PDO object exists
		{
           try {
        $this->db = new PDO ('mysql:host=localhost;dbname=TestDB;charset=utf8', 'root', '');             // Creating new PDO object  
    }
    catch (Exception $e) {              // If not successfull
        echo "Something went wrong.";
    }
            // Create PDO connection
		}
    }
    
    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList()   
    {
		$booklist = array();         
        try{
            $stmt = $this->db->query("SELECT * FROM Book ORDER BY ID");
            foreach ($stmt as $row) {  
            $booklist[] = new Book ($row['Title'],$row['Author'], $row['Description'],
            $row['ID'] );
            }
            return $booklist;
        }
        catch(PDOException $e) {      // In this case an empty booklist is returned
            return $booklist;
        }
        
    }
    
    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */
    public function getBookById($id)    
    {
      // $book initialized to null to return null if the book isn't found
        $book = null;       
        // Checks that the id is valid before preparing statement     
        if(is_numeric($id)){
          try{
            $stmt = $this->db->prepare("SELECT * FROM Book WHERE ID = ?");
        $stmt->execute(array($id)); 
        // Fetches data about the requested book
        $row = $stmt->fetch(PDO::FETCH_ASSOC); 
        if($row){
            $book = new Book ($row['Title'],$row['Author'], $row['Description'], $row['ID'] ); 
            }
        }
        catch(PDOException $e){
            echo "Something went wrong...";
            echo $e->getMessage(); 
        }
        return $book;           
        }
        else {
          $view = new ErrorView('Invalid book id.');
          $view->create(); 
        }

    }
    
    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
     Description is updated to null if empty string.
     Error message if tries to add book without author/title
	 * @throws PDOException
     */
    public function addBook($book)
    {
     if($book->title != '' && $book->author != ''){
      if($book->description == ''){
        $book->description = null; 
      }
        try{
             $stmt = $this->db->prepare("INSERT INTO Book(Title, Author, Description) VALUES (?, ?, ?)");
              
        $stmt->execute(array($book->title, $book->author, $book->description)); 
         $book->id = $this->db->lastInsertId(); 
        }
        catch(PDOException $e){
            $view = new ErrorView();
          $view->create();               
        } 
      }
       else {
          $view = new ErrorView('Title and/or author cannot be empty.');
          $view->create(); 
       }
    }

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     * Title/author cannot be an empty string
     */
    public function modifyBook($book)
    {
      if ($book->title != '' && $book->author != ''){
        try{
            $stmt = $this->db->prepare("UPDATE Book SET Title = ?, Author = ?, Description = ? WHERE id=?");
            $stmt->execute(array($book->title, $book->author, $book->description, $book->id)); 
       }
       catch(PDOException $e){
            echo "Something went wrong...";
            echo $e->getMessage();  
       }
      }
      else {
        $view =  new ErrorView('Title and/or author cannot be empty.'); 
        $view->create(); 
      }      
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {
        try{
            $stmt = $this->db->prepare("DELETE FROM Book WHERE id = ?");
        $stmt->execute(array($id));
        }
        catch(PDOException $e){
            echo "Something went wrong..."; 
            echo $e->getMessage(); 
        }
    }
	
}
  
?>